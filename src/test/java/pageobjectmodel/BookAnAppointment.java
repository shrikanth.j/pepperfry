package pageobjectmodel;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;

public class BookAnAppointment {
	
	WebDriver driver;
	public BookAnAppointment(WebDriver driver) {
		this.driver = driver;
	}
	
	@FindBy(linkText="Buy on Phone")
	private WebElement buyOnPhone;
	
	public void clickBuyOnPhone() {
		buyOnPhone.click();
	}
	
	
	public void switchingWindow() {
		
		Set<String> allWindows = driver.getWindowHandles();
		for(String window :allWindows ) {
				driver.switchTo().window(window);
		//searchStudio.sendKeys(pin);
		}
	}
	
	@FindBy(id="studioSearchInput")
	private WebElement searchStudio;
	
	public void enterPin(String pin) {
		searchStudio.sendKeys(pin);
	}
	
	@FindBy(id="userLocate")
	private WebElement userLocate;
	
	public void clickSearch() {
		userLocate.click();
	}

	@FindBy(xpath="(//a[@class=\"studioBookAppointment\"])[209]")
	private WebElement bookAnAppointmentLink;
	
	public void clickBookAnAppointmentLink() {
		bookAnAppointmentLink.click();
	}
	
	@FindBy(xpath="(//input[@name='name'])[2]")
	private WebElement fullName;
	
	public void enterFullName(String name) {
		fullName.sendKeys(name);
	}
	
	@FindBy(xpath="(//input[@name='phone'])[2]")
	private WebElement phoneNum;
	
	public void enterPhoneNum(String phNo) {
		phoneNum.sendKeys(phNo);
	}
	
	@FindBy(xpath="(//input[@class='bes-email-ip  bes-inpt'])[1]")
	private WebElement email;
	
	public void enterEmail(String email1) {
		email.sendKeys(email1);
	}
	
	@FindBy(id="studioAppointmentDate")
	private WebElement dateWidget;
	
	@FindBy(xpath="//a[@class='ui-datepicker-next ui-corner-all']")
	private WebElement clickNext;
	
	@FindBy(xpath="//a[text()='2']")
	private WebElement clickDate;
	
	public void selectDate() {
		dateWidget.click();
		clickNext.click();
		clickDate.click();
	}
	
	@FindBy(xpath="//select[@class='pf-select studioFormTime select2-hidden-accessible']")
	private WebElement timeSlotList;
	
	@FindBy(xpath="//li[text()='2:00pm to 3:00pm']")
	private WebElement timeSlot;
	
	public void selectTimeSlot() {
		//timeSlotList.click();
		Select sel = new Select(timeSlotList);
		sel.selectByVisibleText("2:00pm to 3:00pm");
		//timeSlotList.click();
		//timeSlot.click();
	}
	
	@FindBy(xpath="//input[@placeholder='Tell us what you are looking for!']")
	private WebElement sendText;
	
	public void enterText(String text) {
		sendText.sendKeys(text);
	}
	
	@FindBy(id="appointment_book_submit")
	private WebElement bookAnAppointmentButton;
	
	public void clickBookAnAppointmentButton() {
		bookAnAppointmentButton.click();
	}
}
