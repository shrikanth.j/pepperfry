package pageobjectmodel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage {

	WebDriver driver;
	public LoginPage(WebDriver driver) {
		this.driver=driver;
	}
	
	@FindBy(id="registerPopupLink")
	private WebElement loginLink;
	
	public void clickLoginLink() {
		loginLink.click();
	}
	
	@FindBy(linkText="Existing User? Log In")
	private WebElement existingUserloginLink;
	
	public void clickExistingUserloginLink() throws InterruptedException {
		Thread.sleep(5000);
		existingUserloginLink.click();
	}
	
	@FindBy(name="user[new]")
	private WebElement userName;
	
	public void eneterUserName(String name) {
		userName.sendKeys(name);
	}
	
	@FindBy(id="password")
	private WebElement password;
	
	public void eneterPassword(String pass) {
		password.sendKeys(pass);
	}
	
	@FindBy(id="formSubmit-popup_login_username_form")
	private WebElement loginButton;
	
	public void clickLoginButton() {
		loginButton.click();
	}

}
