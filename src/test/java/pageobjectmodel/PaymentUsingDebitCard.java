package pageobjectmodel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class PaymentUsingDebitCard {

	WebDriver driver;
	
	public PaymentUsingDebitCard(WebDriver driver) {
		this.driver=driver;
	}
	
	@FindBy(xpath="//[@class='hd-icon-cta hd-cart-cta active']")
	private WebElement cartIcon;
	
	public void clickCartIcon() {
		cartIcon.click();
	}
	
	@FindBy(linkText="Proceed to pay securely ")
	private WebElement proceedToPaySecurelyButton;
	
	public void clickProceedToPaySecurelyButton() {
		proceedToPaySecurelyButton.click();
	}
	
	@FindBy(id="Button")
	private WebElement proceedToPayButton;
	
	public void clickProceedToPayButton() {
		proceedToPayButton.click();
	}
	
	@FindBy(xpath="//[@class='text-xl font-medium marginBottom-12 option-heading']")
	private WebElement debitCard;
	
	public void clickDebitCard() {
		debitCard.click();
	}
	
	@FindBy(id="ccnum")
	private WebElement cardNum;
	
	@FindBy(xpath="//input[@placeholder='Valid Thru (MM/YY)']")
	private WebElement dateYear;
	
	@FindBy(xpath="//input[@formcontrolname='ccvv']")
	private WebElement cvv;
	
	@FindBy(xpath="//input[@placeholder='Name on Card']")
	private WebElement nameOnCard;
	
	@FindBy(id="Button")
	private WebElement payNowButton;
	
	public void enterCardDetails() throws IOException {
		
		 File f = new File("/com.PepperFry/src/test/resources/data/PepperfryData.xlsx");
		 FileInputStream files = new FileInputStream(f);
		 XSSFWorkbook workbook = new XSSFWorkbook(files);
		 XSSFSheet Sheet1 = workbook.getSheetAt(0);
		 
		 int row = Sheet1.getPhysicalNumberOfRows();
		 
		 for(int i=1;i<=row-1;i++) {
			 String cardNumber = Sheet1.getRow(i).getCell(0).getStringCellValue();
			 String monthYear = Sheet1.getRow(i).getCell(1).getStringCellValue();
			 String cvvNumber = Sheet1.getRow(i).getCell(2).getStringCellValue();
			 String nameOnTheCard = Sheet1.getRow(i).getCell(3).getStringCellValue();
			 
			 debitCard.click();
			 cardNum.sendKeys(cardNumber);
			 dateYear.sendKeys(monthYear);
			 cvv.sendKeys(cvvNumber);
			 nameOnCard.sendKeys(nameOnTheCard);
			 payNowButton.click();
		 }
	}
	
}
