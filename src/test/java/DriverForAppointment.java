import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import pageobjectmodel.BookAnAppointment;

public class DriverForAppointment {

	public static void main(String[] args) throws InterruptedException {
		
		WebDriver driver = new ChromeDriver();
		
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		driver.get("https://www.pepperfry.com/");
		
		BookAnAppointment ba = PageFactory.initElements(driver, BookAnAppointment.class);
		
		ba.clickBuyOnPhone();
		ba.switchingWindow();
		ba.enterPin("560001");
		Thread.sleep(2000);
		ba.clickSearch();
		//Thread.sleep(4000);
		ba.clickBookAnAppointmentLink();
		Thread.sleep(2000);
		ba.enterFullName("Shrikanth");
		ba.enterPhoneNum("9876543210");
		ba.enterEmail("shrikanthsj.sindigere@gmail.com");
		ba.selectDate();
		Thread.sleep(2000);
		ba.selectTimeSlot();
		ba.enterText("I am looking for studio's");
		//Thread.sleep(2000);
		ba.clickBookAnAppointmentButton();
		driver.quit();
	}

}
