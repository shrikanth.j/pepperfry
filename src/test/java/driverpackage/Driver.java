package driverpackage;

import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;

import pageobjectmodel.LoginPage;
import pageobjectmodel.PaymentUsingDebitCard;

public class Driver {

	public static void main(String[] args) throws InterruptedException, IOException {
		
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		driver.get("https://www.pepperfry.com/");
		
		LoginPage lp = PageFactory.initElements(driver, LoginPage.class);
		
		PaymentUsingDebitCard pudc = PageFactory.initElements(driver, PaymentUsingDebitCard.class);
		
		/*lp.clickLoginLink();
		lp.clickExistingUserloginLink();
		lp.eneterUserName("shrikanthsj.sindigere@gmail.com");
		lp.eneterPassword("Shreesj@123");
		lp.clickLoginButton();
		*/
		//driver.quit();
		
		Thread.sleep(30000);
		
		pudc.clickCartIcon();
		
		
		pudc.clickProceedToPaySecurelyButton();
		pudc.clickProceedToPayButton();
		pudc.enterCardDetails();
		
	}

}
